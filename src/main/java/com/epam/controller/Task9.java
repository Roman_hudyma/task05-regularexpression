package com.epam.controller;

import com.epam.model.Book;
import com.epam.model.regax.Later;
import com.epam.model.regax.Word;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Task9 implements Command {

	final static Logger logger = LogManager.getLogger();

	public void execute() {
		String text = Book.getFormattedText("SomeText.txt").orElse("EMPTY");
		sortByCharCount(text, 'e').forEach(logger::info);
	}

	private List<String> sortByCharCount(String text, final char ch){
		return Word.stream(text)
				.map(String::toLowerCase)
				.sorted()
				.sorted(Comparator.comparing(w -> Later.find(w, ch).size(), Collections.reverseOrder()))
				.collect(Collectors.toList());
	}
}
