package com.epam.controller;

import com.epam.model.Book;
import com.epam.model.regax.Sentence;
import com.epam.model.regax.Word;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Optional;

public class Task3 implements Command {

	final static Logger logger = LogManager.getLogger();

	public void execute() {
		String text = Book.getFormattedText("SomeText.txt").orElse("EMPTY");
		logger.info(uniqueWordFromFirstSentence(text).get());
	}

	private Optional<String> uniqueWordFromFirstSentence(String text){
		return Sentence.all(text).stream()
				.limit(1)
				.flatMap(Word::stream)
				.filter(word -> Sentence.all(text).stream()
						.skip(1)
						.noneMatch(s -> s.contains(word)))
				.findFirst();
	}
}
