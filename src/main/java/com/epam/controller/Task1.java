package com.epam.controller;

import com.epam.model.Book;
import com.epam.model.regax.Sentence;
import com.epam.model.regax.Word;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Comparator;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class Task1 implements Command {

	final static Logger logger = LogManager.getLogger();

	public void execute() {
		String text = Book.getFormattedText("SomeText.txt").orElse("EMPTY");
		final Optional<Map.Entry<String, Long>> stringLongEntry = maxCountWord(wordCount(text));
		logger.info(stringLongEntry.get().getKey() + " " + countSentence(text, stringLongEntry.get().getKey()));
	}

	private Map<String, Long> wordCount(String text) {
		return Word.stream(text)
				.map(String::toLowerCase)
				.collect(Collectors.groupingBy(word -> word, Collectors.counting()));
	}

	private Optional<Map.Entry<String, Long>> maxCountWord(Map<String, Long> map) {
		return map.entrySet()
				.stream()
				.max(Comparator.comparing(Map.Entry::getValue));
	}

	private long countSentence(String text, final String word) {
		return Sentence.all(text).stream()
				.filter(s -> s.toLowerCase().contains(word))
				.count();
	}

}
