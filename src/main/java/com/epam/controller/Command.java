package com.epam.controller;

public interface Command {

	void execute();
}
