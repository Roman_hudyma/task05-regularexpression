package com.epam.controller;

import com.epam.model.Book;
import com.epam.model.regax.Sentence;
import com.epam.model.regax.Word;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.stream.Collectors;

public class Task5 implements Command {

	final static Logger logger = LogManager.getLogger();

	public void execute() {
		String text = Book.getFormattedText("SomeText.txt").orElse("EMPTY");
		List<String> list = swapVowelWithMaxLengthWord(text);
		final List<String> all = Sentence.all(text);
		for (int i = 0; i < list.size(); i++) {
			logger.info(all.get(i));
			logger.info(list.get(i));
		}
	}


	private List<String> swapVowelWithMaxLengthWord(String text) {
		return Sentence.all(text).stream()
				.map(this::swap)
				.collect(Collectors.toList());
	}

	private String swap(String sentence) {
		String vowelWord = Word.vowel(sentence).findFirst().orElse("EMPTY");
		String maxLength = Sentence.wordWithMaxLength(sentence).orElse("EMPTY");
		if (sentence.indexOf(vowelWord) < sentence.indexOf(maxLength)) {
			sentence = sentence.replaceFirst(maxLength, vowelWord)
					.replaceFirst(vowelWord, maxLength);
		} else {
			sentence = sentence.replaceFirst(vowelWord, maxLength)
					.replaceFirst(maxLength, vowelWord);
		}
		return sentence;
	}
}
