package com.epam.controller;

import com.epam.model.Book;
import com.epam.model.regax.Sentence;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.stream.Collectors;

public class Task11 implements Command {

	final static Logger logger = LogManager.getLogger();

	public void execute() {
		String text = Book.getFormattedText("SomeText.txt").orElse("EMPTY");
		removePartSentence(text, 't', 'e').forEach(logger::info);
	}

	private List<String> removePartSentence(String text, final char start, final char end) {
		return Sentence.all(text).stream()
				.map(sentence -> sentence.replace(Sentence.findMaxPartSentence(sentence, start, end).stream()
						.findFirst()
						.orElse(""), ""))
				.collect(Collectors.toList());
	}
}
