package com.epam.controller;

import com.epam.model.Book;
import com.epam.model.regax.Sentence;
import com.epam.model.regax.Word;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class Task2 implements Command {

	final static Logger logger = LogManager.getLogger();

	@Override
	public void execute() {
		String text = Book.getFormattedText("SomeText.txt").orElse("EMPTY");
		sortByReversValue(countWordInSentence(text)).forEach((k,v) -> logger.info(k + " " + v));
	}

	private Map<String, Long> countWordInSentence(String text) {
		return Sentence.all(text).stream()
				.distinct()
				.collect(Collectors.toMap(k -> k, v -> Word.stream(v).count()));
	}

	private LinkedHashMap<String, Long> sortByReversValue(Map<String, Long> map) {
		return map.entrySet().stream()
				.sorted(Map.Entry.comparingByValue())
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
						(oldValue, newValue) -> oldValue, LinkedHashMap::new));
	}

}
