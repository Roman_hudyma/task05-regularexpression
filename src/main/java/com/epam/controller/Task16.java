package com.epam.controller;

import com.epam.model.Book;
import com.epam.model.regax.Word;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.stream.Collectors;

public class Task16 implements Command {

	final static Logger logger = LogManager.getLogger();

	public void execute() {
		String text = Book.getFormattedText("tSomeText.txt").orElse("EMPTY");
		logger.info(replaceWordTo(text,uniqueWordWithLength(text,4), "Coca-Cola"));
	}

	private List<String> uniqueWordWithLength(String text, final int length) {
		return Word.uniqueStream(text)
				.filter(w -> w.length() == length)
				.collect(Collectors.toList());
	}

	private String replaceWordTo(String text, List<String> words, String newWord){
		String newText = text;
		for (String word : words) {
			newText = newText.replaceAll(word, newWord);
		}
		return newText;
	}
}
