package com.epam.controller;

import com.epam.model.Book;
import com.epam.model.regax.Sentence;
import com.epam.model.regax.Word;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.stream.Collectors;

public class Task4 implements Command {

	final static Logger logger = LogManager.getLogger();

	public void execute() {
		String text = Book.getFormattedText("SomeText.txt").orElse("EMPTY");
		uniqueWordInQuestionSentence(text, 5).forEach(logger::info);
	}

	private List<String> uniqueWordInQuestionSentence(String text, final int length){
		return Sentence.question(text).stream()
				.flatMap(Word::uniqueStream)
				.map(String::toLowerCase)
				.distinct()
				.filter(word -> word.length() == length)
				.collect(Collectors.toList());
	}
}
