package com.epam.controller;

import com.epam.model.Book;
import com.epam.model.regax.Later;
import com.epam.model.regax.Word;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Task8 implements Command {

	final static Logger logger = LogManager.getLogger();

	public void execute() {
		String text = Book.getFormattedText("SomeText.txt").orElse("EMPTY");
		sortByFirstConsonant(text).forEach(logger::info);
	}

	private List<String> sortByFirstConsonant(String text) {
		return Word.stream(text)
				.filter(word -> Later.isVowel(word.charAt(0)))
				.filter(Later::isConsonant)
				.map(String::toLowerCase)
				.sorted(Comparator.comparing(o -> Later.consonant(o).stream()
						.findFirst()
						.orElse("")))
				.collect(Collectors.toList());
	}
}
