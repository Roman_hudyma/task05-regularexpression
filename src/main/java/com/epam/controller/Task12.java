package com.epam.controller;

import com.epam.model.Book;
import com.epam.model.regax.Later;
import com.epam.model.regax.Sentence;
import com.epam.model.regax.Word;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.stream.Collectors;

public class Task12 implements Command {

	final static Logger logger = LogManager.getLogger();

	public void execute() {
		String text = Book.getFormattedText("SomeText.txt").orElse("EMPTY");
		logger.info(removeAllWord(text, findWortToRemove(text, 8)));
	}

	private List<String> findWortToRemove(String text, final int length) {
		return Sentence.all(text).stream()
				.flatMap(sentence -> Word.stream(sentence)
						.filter(w -> (w.length() == length)
								&& (Later.isConsonant(String.valueOf(w.charAt(0))))))
				.distinct()
				.collect(Collectors.toList());
	}

	private String removeAllWord(String text, List<String> words) {
		String newText = text;
		for (String word : words) {
			newText = newText.replaceAll(word, "");
		}
		return newText;
	}
}
