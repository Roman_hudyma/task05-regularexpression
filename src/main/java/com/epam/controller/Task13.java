package com.epam.controller;

import com.epam.model.Book;
import com.epam.model.pojo.WordHolder;
import com.epam.model.regax.Word;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Task13 implements Command {

	final static Logger logger = LogManager.getLogger();

	public void execute() {
		String text = Book.getFormattedText("SomeText.txt").orElse("EMPTY");
		countLaterAndSort(text, 'a').forEach(logger::info);
	}

	private List<WordHolder> countLaterAndSort(String text, final char later){
		return Word.stream(text)
				.map(String::toLowerCase)
				.map(word -> new WordHolder(word, Word.stream(word)
						.mapToLong(wordToChar -> wordToChar.codePoints()
								.filter(ch -> ch == later)
								.count())
						.findFirst()
						.orElse(0)))
				.sorted(Comparator.comparing(WordHolder::getCount,Comparator.reverseOrder())
						.thenComparing(WordHolder::getWord) )
				.collect(Collectors.toList());
	}
}
