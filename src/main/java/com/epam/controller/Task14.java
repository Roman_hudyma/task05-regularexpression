package com.epam.controller;

import com.epam.model.Book;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Task14 implements Command {

	final static Logger logger = LogManager.getLogger();

	public void execute() {
		String text = Book.getFormattedText("SomeText.txt").orElse("EMPTY");
		logger.info(longestPalindrome(text));
	}

	private String longestPalindrome(String text) {
		if (text == null || text.length() < 2) {
			return text;
		}
		int length = text.length();
		boolean[][] isPalindrome = new boolean[length][length];
		int left = 0;
		int right = 0;
		for (int j = 1; j < length; j++) {
			for (int i = 0; i < j; i++) {
				boolean isInnerWordPalindrome = isPalindrome[i + 1][j - 1] || j - i <= 2;
				if (text.charAt(i) == text.charAt(j) && isInnerWordPalindrome) {
					isPalindrome[i][j] = true;
					if (j - i > right - left) {
						left = i;
						right = j;
					}
				}
			}
		}
		return text.substring(left, right + 1);
	}

}
