package com.epam.controller;

import com.epam.model.Book;
import com.epam.model.regax.Word;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.stream.Collectors;

public class Task15 implements Command {

	final static Logger logger = LogManager.getLogger();

	public void execute() {
		String text = Book.getFormattedText("SomeText.txt").orElse("EMPTY");
		logger.info(removeLastLaterFromWordExceptLast(text,findUniqueWord(text)));
	}

	private List<String> findUniqueWord(String text){
		return Word.uniqueStream(text).collect(Collectors.toList());
	}

	private String removeLastLaterFromWordExceptLast(String text, List<String>  uniqueWords){
		String newText = text;
		for (String word : uniqueWords) {
			char lastChar = word.charAt(word.length()-1);
			String newWord = word.replaceAll(String.valueOf(lastChar).toLowerCase(), "")
					.replaceAll(String.valueOf(lastChar).toUpperCase(), "") + lastChar;
			newText = newText.replaceAll(word, newWord);
		}
		return newText;
	}
}
