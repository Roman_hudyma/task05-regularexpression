package com.epam.controller;

import com.epam.model.Book;
import com.epam.model.pojo.SentenceHolder;
import com.epam.model.regax.Sentence;
import com.epam.model.regax.Word;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Task10 implements Command {

	final static Logger logger = LogManager.getLogger();

	public void execute() {
		String text = Book.getFormattedText("SomeText.txt").orElse("EMPTY");
		countWordInSentence(text, Arrays.asList("amy", "and"))
				.forEach(e -> logger.info(e.getSentence() + " " + e.getWord() + "=" + e.getCountWord()));
	}

	private List<SentenceHolder> countWordInSentence(String text, List<String> words) {
		return Sentence.all(text).stream()
				.flatMap(sentence -> words.stream()
						.map(word -> new SentenceHolder(sentence, word, Word.stream(sentence)
								.filter(wordFromSentence -> wordFromSentence.equalsIgnoreCase(word))
								.count())))
				.sorted(Comparator.comparing(SentenceHolder::getCountWord, Collections.reverseOrder()))
				.collect(Collectors.toList());
	}

}
