package com.epam.controller;

import com.epam.model.Book;
import com.epam.model.regax.Word;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.stream.Collectors;

public class Task6 implements Command {

	final static Logger logger = LogManager.getLogger();
	private static char charHolder;

	public void execute() {
		String text = Book.getFormattedText("SomeText.txt").orElse("EMPTY");
		newLaterNewLine(text).forEach(logger::info);
	}

	private List<String> newLaterNewLine(String text){
		return Word.stream(text)
				.map(String::toLowerCase)
				.sorted()
				.map(this::charChecker)
				.collect(Collectors.toList());
	}

	private String charChecker(String word){
		if (charHolder != word.charAt(0)) {
			charHolder = word.charAt(0);
			word = "\t" + word;
		}
		return 	word + " ";
	}
}
