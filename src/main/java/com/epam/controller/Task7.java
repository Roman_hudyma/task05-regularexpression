package com.epam.controller;

import com.epam.model.Book;
import com.epam.model.pojo.WordHolder;
import com.epam.model.regax.Later;
import com.epam.model.regax.Word;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Task7 implements Command {

	final static Logger logger = LogManager.getLogger();

	public void execute() {
		String text = Book.getFormattedText("SomeText.txt").orElse("EMPTY");
		ratioVowelToTotal(text).forEach(w -> logger.info(w.getWord() + " " + w.getCountDouble()));
	}

	private List<WordHolder> ratioVowelToTotal(String text) {
		return Word.stream(text)
				.map(word -> new WordHolder(word, Word.stream(word)
						.mapToDouble(wordToChar -> wordToChar.codePoints()
								.mapToObj(ch -> (char) ch)
								.filter(Later::isVowel)
								.count() / (double) wordToChar.length())
						.findFirst()
						.orElse(0)))
				.sorted(Comparator.comparing(WordHolder::getCountDouble))
				.collect(Collectors.toList());
	}
}
