package com.epam.view;

import com.epam.controller.*;
import com.epam.model.pojo.CommandHolder;
import com.epam.model.pojo.MenuHolder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class MenuController {

	final static Logger logger = LogManager.getLogger();
	private ArrayList<CommandHolder> commandHolders;
	private ResourceBundle messages;
	private List<MenuHolder> locale;
	private Scanner scanner;

	public void start() {
		locale = makeLocale();
		mainMenu();
	}

	public void mainMenu() {
		for (int i = 0; i < locale.size(); i++) {
			logger.trace(i + 1 + " " + locale.get(i).getCommandName());
		}
		int index = scanner();
		if (index >= locale.size()) {
			mainMenu();
		}
		messages = ResourceBundle.getBundle("text", locale.get(index).getLocale());
		commandHolders = makeCommand(messages);
		while (true) {
			commandMenu();
		}
	}

	private void commandMenu() {
		logger.trace("0 " + messages.getString("command.0"));
		for (int i = 0; i < commandHolders.size(); i++) {
			logger.trace(i + 1 + " " + commandHolders.get(i).getName());
		}
		int index = scanner();
		if (index == -1) {
			mainMenu();
		} else if (index >= commandHolders.size()) {
			return;
		} else {
			commandHolders.get(index).getCommand().execute();
		}
	}


	private ArrayList<MenuHolder> makeLocale() {
		ArrayList<MenuHolder> menuHolders = new ArrayList<>();
		menuHolders.add(new MenuHolder("USA", new Locale("en", "US")));
		menuHolders.add(new MenuHolder("Українська", new Locale("uk", "UA")));
		menuHolders.add(new MenuHolder("Japanese", new Locale("ja", "JA")));
		return menuHolders;
	}

	private ArrayList<CommandHolder> makeCommand(ResourceBundle messages) {
		ArrayList<CommandHolder> commandHolders = new ArrayList<>();
		commandHolders.add(new CommandHolder(messages.getString("command.1"), new Task1()));
		commandHolders.add(new CommandHolder(messages.getString("command.2"), new Task2()));
		commandHolders.add(new CommandHolder(messages.getString("command.3"), new Task3()));
		commandHolders.add(new CommandHolder(messages.getString("command.4"), new Task4()));
		commandHolders.add(new CommandHolder(messages.getString("command.5"), new Task5()));
		commandHolders.add(new CommandHolder(messages.getString("command.6"), new Task6()));
		commandHolders.add(new CommandHolder(messages.getString("command.7"), new Task7()));
		commandHolders.add(new CommandHolder(messages.getString("command.8"), new Task8()));
		commandHolders.add(new CommandHolder(messages.getString("command.9"), new Task9()));
		commandHolders.add(new CommandHolder(messages.getString("command.10"), new Task10()));
		commandHolders.add(new CommandHolder(messages.getString("command.11"), new Task11()));
		commandHolders.add(new CommandHolder(messages.getString("command.12"), new Task12()));
		commandHolders.add(new CommandHolder(messages.getString("command.13"), new Task13()));
		commandHolders.add(new CommandHolder(messages.getString("command.14"), new Task14()));
		commandHolders.add(new CommandHolder(messages.getString("command.15"), new Task15()));
		commandHolders.add(new CommandHolder(messages.getString("command.16"), new Task16()));
		return commandHolders;
	}

	private int scanner() {
		int index = Integer.MAX_VALUE;
		try {
			scanner = new Scanner(System.in);
			index = scanner.nextInt() - 1;
		} catch (NoSuchElementException e) {
			logger.error(e);
		}
		return index;
	}

}
