package com.epam.model.pojo;

public class SentenceHolder {

	private String sentence;
	private String word;
	private long countWord;

	public SentenceHolder() {
	}

	public SentenceHolder(String sentence, String word, long countWord) {
		this.sentence = sentence;
		this.word = word;
		this.countWord = countWord;
	}

	public String getSentence() {
		return sentence;
	}

	public void setSentence(String sentence) {
		this.sentence = sentence;
	}

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public long getCountWord() {
		return countWord;
	}

	public void setCountWord(int countWord) {
		this.countWord = countWord;
	}
}
