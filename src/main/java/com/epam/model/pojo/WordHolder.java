package com.epam.model.pojo;

public class WordHolder {

	private String word;
	private long count;
	private double countDouble;

	public WordHolder(String word, long count) {
		this.word = word;
		this.count = count;
	}

	public WordHolder(String word, double countDouble) {
		this.word = word;
		this.countDouble = countDouble;
	}

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}

	@Override
	public String toString() {
		return "WordHolder{" +
				"word='" + word + '\'' +
				", count=" + count +
				'}';
	}

	public double getCountDouble() {
		return countDouble;
	}

	public void setCountDouble(double countDouble) {
		this.countDouble = countDouble;
	}
}
