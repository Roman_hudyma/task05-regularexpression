package com.epam.model.pojo;


import com.epam.controller.Command;

public class CommandHolder {

	private String name;
	private Command command;

	public CommandHolder(String name, Command command) {
		this.name = name;
		this.command = command;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Command getCommand() {
		return command;
	}

	public void setCommand(Command command) {
		this.command = command;
	}
}
