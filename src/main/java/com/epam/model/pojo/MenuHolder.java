package com.epam.model.pojo;

import java.util.Locale;

public class MenuHolder {

	private String commandName;
	private Locale locale;

	public MenuHolder(String commandName, Locale locale) {
		this.commandName = commandName;
		this.locale = locale;
	}

	public String getCommandName() {
		return commandName;
	}

	public void setCommandName(String commandName) {
		this.commandName = commandName;
	}

	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}
}
