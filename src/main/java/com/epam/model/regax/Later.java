package com.epam.model.regax;

import java.util.List;
import java.util.regex.Pattern;

public class Later {

	public static boolean isVowel(char later){
		return Pattern.compile("[aeiouAEIOU]").matcher(String.valueOf(later)).find();
	}

	public static boolean isConsonant(String later){
		return Pattern.compile("[^aeiouAEIOU]").matcher(later).find();
	}

	public static List<String> consonant(String text){
		return MyPattern.compile("[^aeiouAEIOU]", text);
	}

	public static List<String> find(String text, char ch){
		String insert = "["+Character.toUpperCase(ch) + ch +"]";
		return MyPattern.compile(insert, text);
	}


}
