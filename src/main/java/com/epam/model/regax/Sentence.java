package com.epam.model.regax;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

public class Sentence {

	public static List<String> all(String text) {
		return MyPattern.compile("[^.?!]+[.?!]", text);
	}

	public static List<String> question(String text) {
		return MyPattern.compile("[^.?!]+[?]", text);
	}

	public static List<String> findMaxPartSentence(String text, char start, char end) {
		String insert = "[^’']\\b[" + start + "].+[" + end + "]\\b";
		return MyPattern.compile(insert, text);
	}

	public static Optional<String> wordWithMaxLength(String text) {
		return Word.stream(text)
				.sorted(Comparator.comparing(String::length, Comparator.reverseOrder()))
				.findFirst();
	}

	public static String replaceVowelToUnderscore(String text) {
		StringBuilder stringBuilder = new StringBuilder();
		for (char ch : text.toCharArray()) {
			if (Later.isVowel(ch)) {
				stringBuilder.append("_");
			} else {
				stringBuilder.append(ch);
			}
		}
		return stringBuilder.toString();
	}

	public static boolean isSentence(String text) {
		return Pattern.compile("[A-Z][^.?!]+[.?!]").matcher(text).find();
	}

	public static String[] theOrYou(String text) {
		return text.split("the|you");
	}
}
