package com.epam.model.regax;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MyPattern {

	public static List<String> compile(String s, String text){
		final Matcher matcher = Pattern.compile(s).matcher(text);
		List<String> word = new LinkedList<>();
		while (matcher.find()) {
			word.add(text.substring(matcher.start(), matcher.end()).trim());
		}
		return word;
	}
}
