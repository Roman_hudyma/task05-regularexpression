package com.epam.model.regax;

import java.util.stream.Stream;

public class Word {

	public static Stream<String> stream(String text) {
		return MyPattern.compile("\\w+’*'*\\w*", text).stream();
	}

	public static Stream<String> uniqueStream(String text) {
		return MyPattern.compile("(\\w+’*'*\\w*\\b)(?!.*\\1\\b)", text).stream();
	}

	public static Stream<String> vowel(String text) {
		return MyPattern.compile("(\\b[aeiouAEIOU]\\w*)", text).stream();
	}

}
