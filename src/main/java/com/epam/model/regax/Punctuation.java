package com.epam.model.regax;

public class Punctuation {

	public static String removeTabsAndSpaces(String text){
		return text.replaceAll("\\t| +", " ");
	}
}
