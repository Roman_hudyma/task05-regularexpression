package com.epam.model;

import com.epam.model.regax.Punctuation;

import java.io.IOException;
import java.util.Optional;

public class Book {

	public static Optional<String> getFormattedText(String path) {
		try {
			return Optional.of(Punctuation.removeTabsAndSpaces(ReadFile.readText(path)));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return Optional.empty();
	}

}
