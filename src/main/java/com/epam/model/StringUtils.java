package com.epam.model;

public class StringUtils<T> {

	public String concatenate(T... words){
		StringBuilder sb = new StringBuilder();
		for (T word : words) {
			sb.append(word.toString());
		}
		return sb.toString();
	}

}
