package com.epam.model;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class ReadFile {

	public static String readText(String path) throws IOException {
		StringBuilder stringBuilder = new StringBuilder();
		File file = new File(path);
		try (BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8.name()))) {
			String line;
			while ((line = bufferedReader.readLine()) != null) {
				stringBuilder.append("\t"+ line);
			}
		}
		return stringBuilder.toString();
	}

}
